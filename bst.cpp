/*
Name : BST.cpp
Author: Amit Malyala
Description:
A class.
Version: 0.01
Notes:
A data structure for binary search tree without balancing

Reference: 
Book: Data structures and algorithm analysis in C++ - 3rd edition by Mark allen Weiss.

*/
#include "bst.h"
#include <iostream>
#include <stdexcept>

/* Constructor */
BST::BST()
{
	left=nullptr;
	right=nullptr;
	parent=nullptr;
	key=nullptr;
}

/* destructor */
BST::~BST()
{
	// Define it later
}

void* BST::GetAddress(void)
{
	return (this);
}

/* 
Find minimum node 
*/
BST* FindMin(BST* Tnode)
{
	BST* ret=nullptr;
	if (Tnode ==nullptr)
	{
		
	}
	if (Tnode->left == nullptr)
	{
		ret=Tnode;
	}
	else 
	{
		FindMin(Tnode->left);
	}
	return ret;
}


/* 
Find maximum node 
*/
BST* FindMax(BST* Tnode)
{
	BST* ret=nullptr;
	if (Tnode ==nullptr)
	{
		
	}
	else if (Tnode->right == nullptr)
	{
		ret=Tnode;
	}
	else 
	{
		FindMax(Tnode->right);
	}
	return ret;
}
/*
 Component std::size_t strlength(const SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(const SCHAR8 *str)
{
    std::size_t count = 0;
    while (*str)
    {
        count++;
        str++;
    }
    return count;
}
/* create new node */
BST* CreateNewNode(const char* str)
{
	BST* newNode= new BST;
	Memcopy(newNode->key,str);
	newNode->parent=newNode->right =newNode->left=nullptr;
	return newNode;
}

/* Insert newnode */
void InsertNode(const char*str, BST*& Tnode)
{
	
	if (Tnode==nullptr)
	{
		Tnode= CreateNewNode(str);
	}
	
	else if (Tnode->key)
	{
		if ( strcmp(str, Tnode->key) <0)
	   {
		   InsertNode(str,Tnode->left);
	   }   
	    else if ( strcmp(str, Tnode->key) >0)
	   {
		  InsertNode(str,Tnode->right);
	   }
	   else
	   {
	   	 // Duplicate node inserted to right of a node
		 InsertNode(str,Tnode->right);  
	   }
	}
}
/* Search tree for a char string */
void* SearchTree(const char* str, BST* Tnode)
{
	BST* ret =nullptr;
	if (Tnode==nullptr)
	{
		ret=nullptr;
		throw std::runtime_error("Error: String not found in binary search tree");
	}
	else if (Tnode->key)
	{
    	if ( strcmp(str, Tnode->key) <0)
	   {
		   return SearchTree(str,Tnode->left);
	   }
	    else if ( strcmp(str, Tnode->key) >0)
	   {
		   return SearchTree(str,Tnode->right);
	   }
	   else if ( strcmp(str, Tnode->key) ==0)
	   {
		   ret= Tnode;
		   std::cout <<"Found string" << std::endl;
	   }
	}
	return ret;
}

/* Delete binary search tree */
void DeleteTree(BST  *&Tnode)
{
	if (Tnode != nullptr)
	{
		DeleteTree(Tnode->left);
		DeleteTree(Tnode->right);
		delete Tnode;
	}
	
	Tnode =nullptr;
}

/* Function doing a single memcopy from one string to another */
void Memcopy(SCHAR8*& dst, const  SCHAR8* src)
{
    std::size_t Length = strlength(src);
    std::size_t index = 0;
    if (dst !=nullptr)
        delete[] dst;
    dst = new char[Length + 1];
    if (dst)
    {
        for (index = 0; index < Length; index++)
        {
            dst[index] = src[index];
        }
        dst[index] = '\0';
    }
    else
    {
        throw std::bad_alloc();
    }
}

// Comment to integrade. Uncomment to test this module.
#if (0)
SINT32 main(void) 
{
	return 0;
}
#endif
