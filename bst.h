/*
Name : BST.h
Author: Amit Malyala
Description:
A class.
Version: 0.1
Notes:
A data structure for binary search tree without balancing
*/

#ifndef BST_H
#define BST_H
#ifdef __cplusplus

#include "std_types.h"
#include <cstring>

class BST
{
	public:
	SCHAR8* key;
	BST* left;
	BST* right;
	BST* parent;
	BST();
	~BST();
	const SCHAR8* GetKey(void);
	void* GetAddress(void);
};

/*
 Component std::size_t strlength(const SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(const SCHAR8 *str);
/* 
Find minimum node 
*/
BST* FindMin(BST* Tnode);

/* Create new node */
BST* CreateNewNode(const char* str);
/* 
Find maximum node 
*/
BST* FindMax(BST* Tnode);
/* Insert newnode */
void InsertNode(const char*str, BST*& Tnode);

/* Function doing a single memcopy from one string to another */
void Memcopy(SCHAR8*& dst, const  SCHAR8* src);

/* Search tree for a char string */
void* SearchTree(const char* str, BST* Tnode);
/* Delete binary search tree */
void DeleteTree(BST  *&Tnode);
/*
 Component std::size_t strlength(const SCHAR8 *str);
 Arguments: char string
 returns : Length of a string
 Version : 0.1
 Description:
 Returns length of a char string.
 */
std::size_t strlength(const SCHAR8 *str);
#endif 
#endif
