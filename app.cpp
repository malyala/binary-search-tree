/*
Name : app.cpp
Author: Amit Malyala
Description:
A class.
Version: 0.1
Notes:
A data structure for binary search tree without balancing
A precondition is that Root node should be nullptr at init. 
coding Log:
22-07-20. Created intiial version. Project compiles with G++ in ISO C++14
23-07-20 Added void * GetAddress() function. Added InsertNode(), SearchTree(), FindMin(), FindMax(), 
         Memcopy(), strlength(), CreateNewNode() functions. Cleaned up code. Added std::exception for search string not found 
		 and bad_alloc.
		 Project tested to search strings inserted and it works without errors
		 
		 
*/
#include <iostream>
#include "bst.h"

/* execute application */
void BuildTree(void);

/* Function main */
SINT32 main(void) 
{
    BuildTree();
	return 0;
}

/* Execute application*/
void BuildTree(void)
{
	BST* btree =nullptr;
	try 
	{
		InsertNode("amit",btree);
		InsertNode("abb",btree);
		InsertNode("cbb",btree);
		InsertNode("dee",btree);
		InsertNode("hellon",btree);
		InsertNode("helios",btree);
		InsertNode("r1a1's",btree);
		InsertNode("hello",btree);
		std::cout <<"Searching the string: hello" << std::endl;
		BST *b=  static_cast<BST*>(SearchTree("hello",btree));
		if (b)
		{
			std::cout << "Memory address of node containing string: ";
			std::cout << std::hex <<  b;
		}
		DeleteTree(btree);
	}
    catch (std::exception const& e)
    {
        std::cout << e.what() << std::endl;
        DeleteTree(btree);
    }
}
