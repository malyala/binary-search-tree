# README #

This is a simple binary search tree for inserting and searching data. This tree is not balanced like a 
Red black tree or AVL tree. The entire code is in C++14 and it compiles with G++ under ISO C++14.